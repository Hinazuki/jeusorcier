<?php 
session_start();
require_once 'ConnexionBDD.php'; 
require_once 'REQUEST.php';
require_once 'Routeur.php';
require_once 'app.php';
require_once 'Model.php';
require_once 'Utilisateur.php';
function parse_headline($str_chapter) {
    return preg_replace('/^#\s(.+)$/m', '<h1>$1</h1>', $str_chapter);
}
function parse_links($str_chapter) {
    return preg_replace('/\[(.+?)\]\(\?p=(\d+)\)/', '<a href="$2">$1</a>', $str_chapter);
}
function trouverMonstre($str_chapter) {
    preg_match_all('/^-\s\s\s(.+)$/m', $str_chapter, $monstre);
    return $monstre;
}
function trouverStat($tab){
    $Stats=[];
    $StatsMonstre=[];
    for ($i=0;$i<count($tab[0]);$i++)
    {
        $StringMonstre=$tab[0][$i];
        $Stats[$i]=explode(',',$StringMonstre);
        for($j=0;$j<count($Stats[$i]);$j++)
        {
            $StatsMonstre[$i][$j]=explode(':',$Stats[$i][$j]);
        }
    }
    return $StatsMonstre;
}
function enleverMonstre($str_chapter) {   
    return preg_replace('/^-\s\s\s(.+)$/m', ' ', $str_chapter);
}
Model::$pdo=$pdo;
$router = app::bind('router',new Router());
$router->load('routes.php');
$request= app::bind('request',new REQUEST($_GET,$_POST,$_SERVER));
$router->runRoute($request);
