<?php
class REQUEST{
    protected $get;
    protected $post;
    protected $serveur;
    function __construct($get=null,$post=null,$serveur=null){
        $this->get=$get ? $get : $_GET;
        $this->post=$post ? $post : $_POST;
        $this->serveur=$serveur ? $serveur : $_SERVER;
    }
    public function getMethod(){
        return $this->serveur['REQUEST_METHOD'];
    }
    public function getInput($key){
        if(isset($this->get[$key])){
            return strip_tags($this->get[$key]);
        }
    }
    public function getPathInfo(){
        return $this->serveur['PATH_INFO'];
    }
}