<?php
 require_once 'REQUEST.php';
class Router{

 protected $routes;
 public function load($file){
    require_once $file;
 }
/**
 * Register a new road to this router
 * 
 * $method  must be one of http verbs : GET , POST , DELETE...
 * @param string $method the http verbs.
 * @param string $path the path_info part.
 * @param callable $callback handler the road.
 * @return Router this Router instance.
 */
 public function addRoute($method,$path,$callback){
   $this->routes[$method][$path]=$callback;
 }  
/**TODO: document this method */
 public function RunRoute(REQUEST $request){
    
     $path = $request->getPathInfo();
     $method =$request->getMethod();
    $parts = explode('/',$path);
   
    if (count($parts)>2){
        if (isset($this->routes[$method][$path])){
            $callback=$this->routes[$method][$path];
            return $callback();
        }
        $parametre = $parts[2];
        $path = '/'.$parts[1].'/{id}';
    } 

    if (isset($this->routes[$method][$path])){
            $callback=$this->routes[$method][$path];
            if (isset($parametre))
            {
                $callback($parametre); 
            }
            else{
                 $callback();                
            }
     } 
 } 
}