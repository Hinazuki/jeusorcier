<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>chapitre n°<?=$data['numPage']?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../style.css">
    <script type="text/javascript" src="../Combat.js"></script>
</head>
<body class="generale">
    <div class="flex">
    <div class="Text">
        <p><?=$data['chapitre'];?></p>
    </div>
    <!-- voir pour verifier existence des images afficher -->
    <img src="../images/<?=$data['numPage']?>.png" width="20%" >
        <div class="border">
            <h3><strong><?= $data['Perso'][0]->Personnage ?></strong></h3>
            <img src="../<?= $data['Perso'][0]->URL_image ?>" alt="">
            <p>Endurance : <?=$data['Perso'][0]->Endurance?></p>
            <p>Habilité : <?=$data['Perso'][0]->habilité?></p>
            <p>Chance : <?=$data['Perso'][0]->Chance?></p>
        </div>
    </div>
    <div class="flex">
        <?php for($i=0;$i<count($data['monstre']);$i++) {?>
        <div>
            <ul>
                <li><?=substr($data['monstre'][$i][0][0].' ',1, -1)?></li>
                <li>Habilité : <?=$data['monstre'][$i][1][1]?></li>
                <li>Endurance : <?=$data['monstre'][$i][2][1]?></li>
            </ul>
            <div>
                <button onclick="StatPourCombatMonstre(<?=$i?>)">calculer l'Habilité du monstre</button>
                <input id="inputHabiliteMonstre<?=$i?>" value="<?=$data['monstre'][$i][1][1]?>"></input> 
            </div> 
            <div>
                <button onclick="StatPourCombatPersonnage(<?=$i?>)">calculer l'Habilité de votre Personnage</button>
                 <input id="inputHabilitePersonnage<?=$i?>" value="<?=$data['Perso'][0]->habilité?>"></input>
            </div> 
            <button onclick="winOrLoose(<?=$i?>)"> Win or Loose </button>
            </div>
        <?php if ($i==2){
        ?></br><?php
        } } ?>
    </div>  
    <button><a href="/Selection_personnage">Accueil</a></button>
</body>
</html>