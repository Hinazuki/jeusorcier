<?php
class Model{

    static public $pdo;

    protected $data;
    public function __construct($Pseudo=null,$PassWord=null){
        if($Pseudo){
        $stm=static::$pdo->prepare("SELECT * FROM {$this->table} WHERE Pseudo=? and password=?");
        if($stm->execute([$Pseudo,$PassWord])!== false){
            $this->data = $stm->fetch(\PDO::FETCH_ASSOC);
        }
        }
    }
    public function __get($key){
        if ($key === 'table'){
            return static::$table;
        }
        if(isset($this->data[$key])){
            return $this->data[$key];
        }
    }
    public function __Set($key,$value){
        if($key==='__data__'){
            $this->data=$value;
        }else {
            $this->data[$key]=$value;           
        }
    }
    static function creat($data){
        $column="";
        $val="";
        foreach($data as $Key=>$value){
            $column.='`'.$Key.'`,';
            $val.='\''.$value.'\',';
        }
         $column = substr($column,0,strlen($column)-1);
         $val = substr($val,0,strlen($val)-1);
         $stm=static::$pdo->prepare("INSERT INTO feuille_personnage ($column,numPage)
         VALUES ($val,0);");
         $res=$stm->execute();
         if($res!=false){
             return true;
         }
    }
    public function Update(){
        $sets = [];        
        foreach($this->data as $key=>$val){
            if($key!=='id'&&$val!=null){
                $sets[]="$key='$val'";
            }
           
        }
        $sets = 'SET '.implode(',',$sets);
        $stm=static::$pdo->prepare("UPDATE {$this->table} $sets WHERE id_Utilisateur=?");
        $res = $stm->execute([$this->id]);
        if($res!== false){
            return true;
        }
    }
    static function UpdatePage($chap){
        $id=$_SESSION['idPerso'];
        $numPage='SET numPage='.$chap;
        $stm=static::$pdo->prepare("UPDATE feuille_personnage $numPage WHERE id_feuille_personnage=?");
        $res = $stm->execute([$id]);
        if($res!== false){
            return true;
        }
    }
    static function TablePerso($id){
        $stm=static::$pdo->prepare("SELECT * FROM feuille_personnage where id_Utilisateur=?");
        if($stm->execute([$id])!== false){
            $results = [];
            foreach($stm->fetchAll(\PDO::FETCH_ASSOC) as $row){
                $model=new static();
                $model->__data__=$row;
                $results[]=$model;
            }
            return $results;
        }
    }
    static function Perso($idPerso,$idUtilisateur){
        $stm=static::$pdo->prepare("SELECT * FROM feuille_personnage where id_Utilisateur=:idutilisateur and id_feuille_personnage=:idPerso");
        if($stm->execute([':idutilisateur'=>$idUtilisateur,':idPerso'=>$idPerso])!== false){
            $results = [];
            foreach($stm->fetchAll(\PDO::FETCH_ASSOC) as $row){
                $model=new static();
                $model->__data__=$row;
                $results[]=$model;
            }
            return $results;
        }
    }
    public static function all(){
        $table = static::$table;
        $stm=static::$pdo->query("SELECT * FROM {$table}");
        if($stm!== false){
            $results = [];
            foreach($stm->fetchAll(\PDO::FETCH_ASSOC) as $row){
                $model=new static();
                $model->__data__=$row;
                $results[]=$model;
            }
            return $results;
        }
    }
    public function toArray(){
        return $this->data;
    }
}