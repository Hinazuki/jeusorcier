<?php
class view{
    protected $file;

    public function __construct($file){
        $file = $file.'.php';
        if(file_exists($file)){
           $this->file=$file;
        }
        else{
            throw new InvalidArgumentException("le fichier n'existe pas");
        }
    }
    public function render($data=[]){
        extract($data);
        include $this->file;
    }
}