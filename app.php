<?php 
// namespace WebFrame;
class app{
    protected static $registy;
    static function bind($key,$value){
        static::$registy[$key]=$value;
        return $value;
    }
    static function get($key){
        if(!isset(static::$registy[$key])){
            throw new Exception("key {$key} not bound in this container");
        }
        return static::$registy[$key];
    }
}