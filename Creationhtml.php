<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Creation de Personnage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="generale">
<div class="creation">
<form method="POST" action="/creation">
    <label for="Personnage">Pseudo :</label>
    <input id="Personnage" name="Personnage" required>
    <Label for="URL_image"><br>Apparence du Personnage<br></Label>
    <input type="radio" name="URL_image" value="Image_Personnage/Personnage1.png">
    <img src="Image_Personnage/Personnage1.png" width="100" height="100">
    <input type="radio" name="URL_image" value="Image_Personnage/Personnage2.png">
    <img src="Image_Personnage/Personnage2.png" width="100" height="100">   
    <label for="Habilité"><br>Habilité :</label>
    <input id="Habilité" name="Habilité" value="<?=rand(1,6)+6?>" readonly>
    <label for="Endurance"><br>Endurance :</label>
    <input id="Endurance" name="Endurance" value="<?=rand(1,6)+12?>" readonly>
    <label for="Chance"><br>Chance :</label>
    <input id="Chance" name="Chance" value="<?=rand(1,6)+6?>" readonly>
    <input  type="submit" value="Creation">
</form>
</div>
</body>
</html>