<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Selection Personnage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styleSelection.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="generale">
<ul>
    <?php foreach($data as $Personnage):?>
        <li>       
            <strong><?=$Personnage->Personnage?></strong>
            <a href="/Personnage/<?=$Personnage->id_feuille_personnage ?>">
            <img src=<?=$Personnage->URL_image?> alt="" />
            <div>
            <strong>Habilité:<?=$Personnage->habilité ?></strong>
            <strong>Endurance:<?=$Personnage->Endurance ?></strong> 
            <strong>Chance:<?=$Personnage->Chance ?></strong>
            <strong>Page:<?=$Personnage->numPage ?></strong>
            </div>
            </a>                               
        </li>
     <?php endforeach?>
</ul> 
<button><a href="/creation">creation de personnage</a></button>
</body>
</html>