<?php 
require_once 'app.php';
require_once 'View.php';

$this->addRoute('GET','/connexion',function(){
    global $pdo;
            (new View('Connexionhtml'))->render();              
});

$this->addRoute('POST','/connexion',function(){
    global $pdo;
    $Utilisateur = new Utilisateur($_POST['Pseudo'],$_POST['PassWord']);
    if($Utilisateur!==false){
        $_SESSION['Utilisateur']=$Utilisateur->idutilisateur;
        if($_SESSION['Utilisateur']!=null){
            header('Location: /Selection_personnage');
        }    
    }
});

$this->addRoute('GET','/jouer/{id}',function($chap){
    $str_book = file_get_contents('le-sorcier-de-la-montagne-de-feu.md');
    $str_chapters = preg_split("/---/", $str_book);
    $chapitre=parse_headline($str_chapters[$chap]); 
    $chapitre=parse_links($chapitre);
    $monstre = trouverMonstre($chapitre);
    $StatsMonstre = trouverStat($monstre);
    $chapitre = enleverMonstre($chapitre);
    $Perso=Utilisateur::Perso($_SESSION['idPerso'],$_SESSION['Utilisateur']);
    (new View('Jouerhtml'))->render(['chapitre'=>$chapitre,
                                     'numPage'=>$chap,
                                     'Perso'=>$Perso,
                                     'monstre'=>$StatsMonstre]);
    Utilisateur::UpdatePage($chap);
});
$this->addRoute('GET','/Personnage/{id}',function($idperso){
        $Perso=Utilisateur::Perso($idperso,$_SESSION['Utilisateur']);
        $_SESSION['idPerso']=$idperso;
        $_SESSION['numPage']=$Perso->numPage;
        $numPage='Location: /jouer/'.$Perso[0]->numPage;
        if($Perso!=null){
             header($numPage);
        }
});
$this->addRoute('GET','/Selection_personnage',function(){
    if($_SESSION['Utilisateur']!=null){ 
        $ListePerso=Utilisateur::TablePerso($_SESSION['Utilisateur']); 
        (new View('Selectionhtml'))->render($ListePerso);
    } 
});

$this->addRoute('GET','/creation',function(){
    (new View('Creationhtml'))->render();
});
$this->addRoute('POST','/creation',function(){
    $newPerso['Id_Utilisateur']=$_SESSION['Utilisateur'];  
    $newPerso+=$_POST;
    if(Utilisateur::creat($newPerso)){
        header('Location: /Selection_personnage'); 
    }
    else{
        header('Location: /creation');
    }
});