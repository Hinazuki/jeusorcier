<?php 
    error_reporting(E_ALL);
    ini_set("display_errors",1); 
?>
<?php 
require 'vendor/autoload.php';
use \DebugBar\StandardDebugBar;
$debugbar = new StandardDebugBar();
$renderer = $debugbar->getJavascriptRenderer();
$renderer -> renderOnShutdownWithHead();
$routes = [];

function Debug($msg){
    global $debugbar;
    $debugbar['messages']->debug($msg);
}
 function generate_URL($str_route,$params=[]){
     if (is_array($str_route)){
         $str_route=implode('/',$str_route);
     }

     $params=array_merge($_GET,$params);

     $str_route=$str_route.'?';

     foreach($params as $key =>$value){
        $str_route.$key.'='.$value.'&';
     }
     return $str_route;
 }
$host = '127.0.0.1';
$user = 'root';
$pswd = 'root';
$dbname= 'jeusorcier';
try{
    $pdo = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8",$user,$pswd);
}catch(Exception $e){
    die('erreur de connexion');
}